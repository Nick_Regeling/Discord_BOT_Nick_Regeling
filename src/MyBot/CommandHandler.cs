﻿using System.Threading.Tasks;
using System.Reflection;
using Discord.Commands;
using Discord.WebSocket;
using System.Linq;
using MySql.Data.MySqlClient;
using MyBot.Modules.Public;
using Discord;

namespace MyBot.Modules.Public
{
    public class CommandHandler
    {
        private CommandService _cmds;
        private DiscordSocketClient _client;

        public async Task Install(DiscordSocketClient c)
        {
            _client = c;
            _cmds = new CommandService();

            await _cmds.AddModulesAsync(Assembly.GetEntryAssembly());

            _client.MessageReceived += HandleCommand;
            _client.Ready += gameChange;
            _client.UserJoined += UserJoined;


        }

        public async Task gameChange()
        {
            await _client.SetGameAsync("Salsa Sauce");
        }

        public async Task UserJoined(SocketGuildUser user)
        {           
            var channel = user.Guild.DefaultChannel;
            await channel.SendMessageAsync("Welcome to the server " + user.Mention + "!");

            var result = Database.CheckExistingUser(user);
            if (result.Count() <= 0)
            {
                Database.EnterUser(user);
            }
        }



        public async Task HandleCommand(SocketMessage s)
        {
            var msg = s as SocketUserMessage;
            if (msg == null) return;

            var context = new CommandContext(_client, msg);

            int argPos = 0;
            if (msg.HasStringPrefix("!", ref argPos) || msg.HasMentionPrefix(_client.CurrentUser, ref argPos))
            {
                var result = await _cmds.ExecuteAsync(context, argPos);

                if (!result.IsSuccess) await context.Channel.SendMessageAsync(result.ToString());
            }
        }
    }
}