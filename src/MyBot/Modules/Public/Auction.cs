﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace MyBot.Modules.Public
{
    public class Auction : ModuleBase
    {
        static string auctionCheck = "";
        static ulong currentAuction = 0;
        static int hightBid;
        static SocketGuildUser highBidder = null;
        static string currentItem;

        [Command("auction")]
        public async Task AuctionAsync(int startingBid, int amount, string item, [Remainder]string info = null)
        {
            if (auctionCheck == "" || auctionCheck == "over") auctionCheck = "live";
            else if (auctionCheck == "live")
            {
                var messageToDel = await ReplyAsync("Auction already started, only one at a time please");
                DelayDeleteMessage(Context.Message, 10);
                DelayDeleteMessage(messageToDel, 10);
                return;
            }
            var embed = new EmbedBuilder()
            {
                Color = new Color(96, 255, 185)
            };
            embed.AddField(x =>
            {
                x.Name = $"Auction for {item} started {DateTime.UtcNow} UTC";
                x.Value = $"{amount} x {item} is up for auction with starting bid of {startingBid}\nType !bid [amount] to bid";
            });
            var message = await ReplyAsync("", embed: embed);
            hightBid = startingBid - 1;
            currentItem = item;
            currentAuction = message.Id;
        }

        [Command("auctionEnd")]
        public async Task AuctionOverAsync()
        {
            if (highBidder == null)
            {
                await Context.Channel.SendMessageAsync("The auction has ended with 0 bids.");
                auctionCheck = "over";
                hightBid = 0;
                currentItem = null;
                currentAuction = 0;
                return;
            }
            var embed = new EmbedBuilder()
            {
                Color = new Color(96, 255, 185)
            };
            embed.Title = $"{highBidder} won the auction for {currentItem}";
            embed.AddField(x =>
            {
                x.Name = $"Auction ended at {DateTime.UtcNow}";
                x.Value = $"Once you pay {hightBid}, we will arrange payment and delivery of {currentItem} soon after. Congratz! :tada:";
                x.IsInline = false;
            });

            auctionCheck = "over";
            highBidder = null;
            hightBid = 0;
            currentItem = null;
            currentAuction = 0;
            await Context.Channel.SendMessageAsync("", false, embed);
        }

        [Command("auctionCheck")]
        public async Task AuctionCheckAsync()
        {
            var DM = await Context.User.GetOrCreateDMChannelAsync();
            if (auctionCheck == "over" || auctionCheck == "")
            {
                var message = await DM.SendMessageAsync("Sorry there isn't an action at this time");
                DelayDeleteMessage(Context.Message, 10);
                DelayDeleteMessage(message, 10);
                return;
            }
            var AuctionStatus = Context.Channel.GetMessageAsync(currentAuction);
            var embed = AuctionStatus.Result.Embeds.FirstOrDefault() as Embed;
            await DM.SendMessageAsync("", false, embed);
            DelayDeleteMessage(Context.Message, 10);
        }

        [Command("bid")]
        public async Task BidAsync(string amount)
        {
            int bid;
            if (auctionCheck != "live")
            {
                var message = await ReplyAsync("There's no auction at this time, ask someone to start one.");
                DelayDeleteMessage(Context.Message, 10);
                DelayDeleteMessage(message, 10);
                return;
            }
            else
            {
                try
                {
                    bid = Math.Abs(int.Parse(amount));
                }
                catch
                {
                    var message = await ReplyAsync("That's not a valid bid, I'm not stupid.");
                    DelayDeleteMessage(Context.Message, 10);
                    DelayDeleteMessage(message, 10);
                    return;
                }
                if (bid <= hightBid)
                {
                    var message = await ReplyAsync("Your bid is too low, increase it and try again.");
                    DelayDeleteMessage(Context.Message, 10);
                    DelayDeleteMessage(message, 10);
                    return;
                }

                hightBid = bid;
                highBidder = Context.User as SocketGuildUser;

                await UpDateHighBidder(Context.Message as SocketUserMessage, bid);
                var message2 = await ReplyAsync($"The current highest bidder is {highBidder.Mention} with a bid of {hightBid} :moneybag:");
                DelayDeleteMessage(Context.Message, 10);
                DelayDeleteMessage(message2, 10);
            }
        }

        private async Task UpDateHighBidder(SocketUserMessage messageDetails, int bid)
        {
            var exactMessage = await messageDetails.Channel.GetMessageAsync(currentAuction) as IUserMessage;
            var embed2 = new EmbedBuilder()
            {
                Color = new Color(96, 255, 185)
            };
            var oldField = exactMessage.Embeds.FirstOrDefault().Fields.FirstOrDefault();
            embed2.AddField(x =>
            {
                x.Name = oldField.Name;
                x.Value = oldField.Value;
                x.IsInline = oldField.Inline;
            });

            embed2.AddField(x =>
            {
                x.Name = "New High Bid!";
                x.IsInline = false;
                x.Value = $"{currentItem} highest bid is {bid} by {highBidder}";
            });
        }

        private async Task DelayDeleteMessage(IUserMessage message, int time = 0)
        {
            var delete = Task.Run(async () =>
            {
                if (time == 0) await Task.Delay(2500);
                else await Task.Delay(time * 1000);
                await message.DeleteAsync();
            });
        }
    }
}

