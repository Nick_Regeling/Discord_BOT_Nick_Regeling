﻿using Discord;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using MySql.Data.MySqlClient;

namespace MyBot.Modules.Public
{
    public class Database
    {
        private string table { get; set; }
        private const string server = "127.0.0.1";
        private const string database = "spaghettidb";
        private const string username = "root";
        private const string password = "";
        private MySqlConnection dbConnection;

        public Database(string table)
        {
            this.table = table;
            MySqlConnectionStringBuilder stringBuilder = new MySqlConnectionStringBuilder();
            stringBuilder.Server = server;
            stringBuilder.UserID = username;
            stringBuilder.Password = password;
            stringBuilder.Database = database;
            stringBuilder.SslMode = MySqlSslMode.None;

            var connectionString = stringBuilder.ToString();

            dbConnection = new MySqlConnection(connectionString);

            dbConnection.Open();
        }

        public MySqlDataReader FireCommand(string query)
        {
            if (dbConnection == null)
            {
                return null;
            }

            MySqlCommand command = new MySqlCommand(query, dbConnection);

            var mySqlReader = command.ExecuteReader();

            return mySqlReader;
        }

        public void CloseConnection()
        {
            if (dbConnection != null)
            {
                dbConnection.Close();
            }
        }

        public static List<String> CheckExistingUser(IUser user)
        {
            var result = new List<String>();
            var database = new Database("spaghettidb");

            var str = string.Format("SELECT * FROM spaghettidata WHERE user_id = '{0}'", user.Id);
            var tableName = database.FireCommand(str);

            while (tableName.Read())
            {
                var userId = (string)tableName["user_id"];

                result.Add(userId);
            }

            return result;
        }

        public static string EnterUser(IUser user)
        {
            var database = new Database("spaghettidb");

            var str = $"INSERT INTO spaghettidata (user_id, username, tokens, level, xp) VALUES ('{user.Id}', '{user.Username}', '100', '1', '1')";
            var table = database.FireCommand(str);

            database.CloseConnection();

            return null;
        }

        public static List<tableName> GetUserStatus(IUser user)//might be wrong?
        {
            var result = new List<tableName>();//might be wrong?

            var database = new Database("spaghettidb");

            var str = $"SELECT * FROM spaghettidata WHERE user_id = '{user.Id}'";
            var tableName = database.FireCommand(str);

            while (tableName.Read())
            {
                var userId = (string)tableName["user_id"];
                var userName = (string)tableName["username"];
                var currentTokens = (int)tableName["tokens"];
                var level = (int)tableName["level"];
                var xp = (int)tableName["xp"];
                result.Add(new tableName //might be wrong?
                {
                    UserId = userId,
                    Username = userName,
                    Tokens = currentTokens,
                    level = level,
                    XP = xp
                });
            }
            database.CloseConnection();

            return result;

        }

        public static void ChangeTokens (IUser user, int tokens)
        {
            var database = new Database("spaghettidb");

            try
            {
                var strings = string.Format("UPDATE spaghettidata SET tokens = tokens + '{1}' WHERE user_id = {0}", user.Id, tokens);
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;                   
            } catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }

        public static void addXP(IUser user, int xp)
        {
            var database = new Database("spaghettidb");
            try
            {
                var strings = $"UPDATE spaghettidata SET xp = xp + {xp} WHERE user_id = {user.Id.ToString()}";
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;
            } catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }

        public static void levelUp(IUser user, int xp)
        {
            var database = new Database("spaghettidb");

            try
            {
                var strings = $"UPDATE spaghettidata SET level = level + {1}, xp = xp + {xp} WHERE user_id = {user.Id.ToString()}";
                var reader = database.FireCommand(strings);
                reader.Close();
                database.CloseConnection();
                return;
            } catch (Exception e)
            {
                database.CloseConnection();
                return;
            }
        }
    }
}