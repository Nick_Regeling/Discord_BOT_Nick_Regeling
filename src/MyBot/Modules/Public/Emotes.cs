﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System.Runtime.InteropServices;
using System.Diagnostics;
using Discord.Audio;

namespace MyBot.Modules.Public
{
    public class Emotes : ModuleBase
    {
        [Command("Moon2Choke")]
        [Alias("m2choke")]
        public async Task eChoke()
        {
            int Delete = 1;
            foreach (var Item in await Context.Channel.GetMessagesAsync(Delete).Flatten())
            {
                await Item.DeleteAsync();
            }

            var builder = new EmbedBuilder()
            {
                Color = new Color(96, 255, 185)
            };
            builder.Title = ("Moon2Choke");
            builder.ImageUrl = ("https://www.dropbox.com/s/31em9iq76zdyosk/Moon2Choke.png?dl=1");

            await ReplyAsync("", false, builder.Build());
        }

        [Command("Moon2Creep")]
        [Alias("m2creep")]
        public async Task eCreep()
        {
            int Delete = 1;
            foreach (var Item in await Context.Channel.GetMessagesAsync(Delete).Flatten())
            {
                await Item.DeleteAsync();
            }

            var builder = new EmbedBuilder()
            {
                Color = new Color(96, 255, 185)
            };
            builder.Title = ("Moon2Creep");
            builder.ImageUrl = ("https://www.dropbox.com/s/z36wu9zawasgdxx/Moon2Creep.png?dl=1");

            await ReplyAsync("", false, builder.Build());
        }

        [Command("Moon2Smeg")]
        [Alias("m2smeg")]
        public async Task eSmeg()
        {
            int Delete = 1;
            foreach (var Item in await Context.Channel.GetMessagesAsync(Delete).Flatten())
            {
                await Item.DeleteAsync();
            }

            var builder = new EmbedBuilder()
            {
                Color = new Color(96, 255, 185)
            };
            builder.Title = ("Moon2Smeg");
            builder.ImageUrl = ("https://www.dropbox.com/s/ba50p8ulnqwlbzb/Moon2smeg.png?dl=1");

            await ReplyAsync("", false, builder.Build());
        }

        [Command("Moon2W")]
        [Alias("m2w")]
        public async Task eW()
        {
            int Delete = 1;
            foreach (var Item in await Context.Channel.GetMessagesAsync(Delete).Flatten())
            {
                await Item.DeleteAsync();
            }

            var builder = new EmbedBuilder()
            {
                Color = new Color(96, 255, 185)
            };
            builder.Title = ("Moon2W");
            builder.ImageUrl = ("https://www.dropbox.com/s/w7platymm1m85vk/Moon2W.png?dl=1");

            await ReplyAsync("", false, builder.Build());
        }

        [Command("Moon2Welp")]
        [Alias("m2welp")]
        public async Task eWelp()
        {
            int Delete = 1;
            foreach (var Item in await Context.Channel.GetMessagesAsync(Delete).Flatten())
            {
                await Item.DeleteAsync();
            }

            var builder = new EmbedBuilder()
            {
                Color = new Color(96, 255, 185)
            };
            builder.Title = ("Moon2Welp");
            builder.ImageUrl = ("https://www.dropbox.com/s/fczvpddkg1w7txc/Moon2Welp.png?dl=1");

            await ReplyAsync("", false, builder.Build());
        }

        [Command("Moon2WHUT")]
        [Alias("m2whut")]
        public async Task eWhut()
        {
            int Delete = 1;
            foreach (var Item in await Context.Channel.GetMessagesAsync(Delete).Flatten())
            {
                await Item.DeleteAsync();
            }

            var builder = new EmbedBuilder()
            {
                Color = new Color(96, 255, 185)
            };
            builder.Title = ("Moon2Whut");
            builder.ImageUrl = ("https://www.dropbox.com/s/kenbvxu07epduzz/Moon2WHUT.png?dl=1");

            await ReplyAsync("", false, builder.Build());
        }

        [Command("PogTrace")]
        [Alias("pog")]
        public async Task ePog()
        {
            int Delete = 1;
            foreach (var Item in await Context.Channel.GetMessagesAsync(Delete).Flatten())
            {
                await Item.DeleteAsync();
            }

            var builder = new EmbedBuilder()
            {
                Color = new Color(96, 255, 185)
            };
            builder.Title = ("PogTrace");
            builder.ImageUrl = ("https://www.dropbox.com/s/3safdnugsas4dqu/PogTrace.png?dl=1");

            await ReplyAsync("", false, builder.Build());
        }

        [Command("seagDino")]
        [Alias("dino")]
        public async Task eDino()
        {
            int Delete = 1;
            foreach (var Item in await Context.Channel.GetMessagesAsync(Delete).Flatten())
            {
                await Item.DeleteAsync();
            }

            var builder = new EmbedBuilder()
            {
                Color = new Color(96, 255, 185)
            };
            builder.Title = ("seagDino");
            builder.ImageUrl = ("https://www.dropbox.com/s/lm020n7lu7u7gtn/seagDino.png?dl=1");

            await ReplyAsync("", false, builder.Build());
        }

        [Command("seagGasm")]
        [Alias("gasm")]
        public async Task eGasm()
        {
            int Delete = 1;
            foreach (var Item in await Context.Channel.GetMessagesAsync(Delete).Flatten())
            {
                await Item.DeleteAsync();
            }

            var builder = new EmbedBuilder()
            {
                Color = new Color(96, 255, 185)
            };
            builder.Title = ("seagGasm");
            builder.ImageUrl = ("https://www.dropbox.com/s/dq3whb3rlrpxpob/seagGasm.png?dl=1");

            await ReplyAsync("", false, builder.Build());
        }

        [Command("seagGotem")]
        [Alias("gotem")]
        public async Task eGotem()
        {
            int Delete = 1;
            foreach (var Item in await Context.Channel.GetMessagesAsync(Delete).Flatten())
            {
                await Item.DeleteAsync();
            }

            var builder = new EmbedBuilder()
            {
                Color = new Color(96, 255, 185)
            };
            builder.Title = ("seagGotem");
            builder.ImageUrl = ("https://www.dropbox.com/s/40mg0zowv1obwfy/seagGotem.png?dl=1");

            await ReplyAsync("", false, builder.Build());
        }

        [Command("seagLUL")]
        [Alias("lul")]
        public async Task eLUL()
        {
            int Delete = 1;
            foreach (var Item in await Context.Channel.GetMessagesAsync(Delete).Flatten())
            {
                await Item.DeleteAsync();
            }

            var builder = new EmbedBuilder()
            {
                Color = new Color(96, 255, 185)
            };
            builder.Title = ("seagLUL");
            builder.ImageUrl = ("https://www.dropbox.com/s/ux1czpgijzw4qdr/seagLUL.png?dl=1");

            await ReplyAsync("", false, builder.Build());
        }

        [Command("seagMei")]
        [Alias("mei")]
        public async Task eMei()
        {
            int Delete = 1;
            foreach (var Item in await Context.Channel.GetMessagesAsync(Delete).Flatten())
            {
                await Item.DeleteAsync();
            }

            var builder = new EmbedBuilder()
            {
                Color = new Color(96, 255, 185)
            };
            builder.Title = ("seagMei");
            builder.ImageUrl = ("https://www.dropbox.com/s/9tcan5iu761p3d3/seagMei.png?dl=1");

            await ReplyAsync("", false, builder.Build());
        }

        [Command("seagWhy")]
        [Alias("why")]
        public async Task eWhy()
        {
            int Delete = 1;
            foreach (var Item in await Context.Channel.GetMessagesAsync(Delete).Flatten())
            {
                await Item.DeleteAsync();
            }

            var builder = new EmbedBuilder()
            {
                Color = new Color(96, 255, 185)
            };
            builder.Title = ("seagWhy");
            builder.ImageUrl = ("https://www.dropbox.com/s/2kafsxg7qj7or65/seagWhy.png?dl=1");

            await ReplyAsync("", false, builder.Build());
        }

        [Command("Moon2Smug")]
        [Alias("m2s")]
        public async Task eM2S()
        {
            int Delete = 1;
            foreach (var Item in await Context.Channel.GetMessagesAsync(Delete).Flatten())
            {
                await Item.DeleteAsync();
            }

            var builder = new EmbedBuilder()
            {
                Color = new Color(96, 255, 185)
            };
            builder.Title = ("Moon2Smug");
            builder.ImageUrl = ("https://www.dropbox.com/s/ts2xyn5is5epbc9/smug.png?dl=1");

            await ReplyAsync("", false, builder.Build());
        }

        [Command("thicc")]
        [Alias("datass")]
        public async Task ethicc()
        {
            int Delete = 1;
            foreach (var Item in await Context.Channel.GetMessagesAsync(Delete).Flatten())
            {
                await Item.DeleteAsync();
            }

            var builder = new EmbedBuilder()
            {
                Color = new Color(96, 255, 185)
            };
            builder.Title = ("seagTHICC");
            builder.ImageUrl = ("https://www.dropbox.com/s/fg3d2j1mtlsp55f/THICCC.png?dl=1");

            await ReplyAsync("", false, builder.Build());
        }
    }
}
