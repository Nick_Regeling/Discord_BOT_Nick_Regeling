﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System.Runtime.InteropServices;
using System.Diagnostics;
using Discord.Audio;

namespace MyBot.Modules.Public
{

    public class General : ModuleBase
    {
        [Command("Owner")]
        public async Task ownerCommand()
        {
            await Context.Channel.SendMessageAsync("Ik ben gemaakt door: Nick Regeling!");
        }
  
        [Command("Stare")]
        [Alias("stare")]
        public async Task scarystare(SocketGuildUser user)
        {
            await Context.Channel.SendMessageAsync(Context.User.Mention + " " + "is staring at" + " " + user.Mention + "\n");
            await Context.Channel.SendFileAsync("C:/img/scarystare.gif");
        }

        [Command("Skrr")]
        [Alias("skrrskrr", "Skr")]
        public async Task Skrrr()
        {
            await Context.Channel.SendFileAsync("C:/img/SKRR.gif");
        }

        string[] rText = new string[] 
            {
                "That's one cute pussy isn't it?",
                "Aww look at it :heart_eyes:",
                "AWW HELL NAWW, KEEP IT AWAY FROM ME",
                "You just typed !pussy because you can't get any....",
                "That's a nice kitty right there"

            };
          

        [Command("quote")]
        [Alias("ps")]
        [Summary("Sends a nice pussy in the chat")]
        public async Task Quote()
        {
            int Delete = 1;
            foreach (var Item in await Context.Channel.GetMessagesAsync(Delete).Flatten())
            {
                await Item.DeleteAsync();
            }
            Console.WriteLine("Making API Call...");
            using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
            {
                string websiteurl = $"https://dog.ceo/api/breeds/image/random";
                client.BaseAddress = new Uri(websiteurl);
                HttpResponseMessage response = client.GetAsync("").Result;
                response.EnsureSuccessStatusCode();
                string result = await response.Content.ReadAsStringAsync();
                var json = JObject.Parse(result);
                string Quote = json["message"].ToString();
                await ReplyAsync(Quote);
            }
        }

        [Command("Main")]
        [Summary("Randomly picks ur OW main")]
        [Alias("main")]
        public async Task Main()
        {
            Random rand = new Random();
            int rHero = rand.Next(1, 25);

            switch (rHero)
            {
                case 1:
                    await ReplyAsync("Get in there, you're powered up!" + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Ana.png");
                    break;
                case 2:
                    await ReplyAsync("Beep Boop Beep." + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Bastion.png");
                    break;
                case 3:
                    await ReplyAsync("Only through conflict do we evolve." + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Doomfist.png");
                    break;
                case 4:
                    await ReplyAsync("Winky Face ;)" + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Dva.png");
                    break;
                case 5:
                    await ReplyAsync("I need healing!" + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Genji.png");
                    break;
                case 6:
                    await ReplyAsync("The dragon awakens." + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Hanzo.png");
                    break;
                case 7:
                    await ReplyAsync("It's a perfect day for some mayhem." + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Junkrat.png");
                    break;
                case 8:
                    await ReplyAsync("Have some Lucio Oh's!" + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Lucio.png");
                    break;
                case 9:
                    await ReplyAsync("It's" + DateTime.UtcNow + "\n");
                    await Context.Channel.SendFileAsync("C:/img/McCree.png");
                    break;
                case 10:
                    await ReplyAsync("Freeze don't move!" + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Mei.png");
                    break;
                case 11:
                    await ReplyAsync("Heroes never die!" + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Mercy.png");
                    break;
                case 12:
                    await ReplyAsync("Your safety is my primary concern." + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Orisa.png");
                    break;
                case 13:
                    await ReplyAsync("Justice rains from above" + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Pharah.png");
                    break;
                case 14:
                    await ReplyAsync("Die Die Die!" + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Reaper.png");
                    break;
                case 15:
                    await ReplyAsync("Precision German engineering" + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Reinhardt.png");
                    break;
                case 16:
                    await ReplyAsync("Say 'Bacon' one more time" + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Roadhog.png");
                    break;
                case 17:
                    await ReplyAsync("I've got you in my sights" + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Soldier.png");
                    break;
                case 18:
                    await ReplyAsync("Everything can be hacked..and everyone" + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Sombra.png");
                    break;
                case 19:
                    await ReplyAsync("Why do you struggle?" + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Symmetra.png");
                    break;
                case 20:
                    await ReplyAsync("Hard work pays off." + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Torbjorn.png");
                    break;
                case 21:
                    await ReplyAsync("Ever get that feeling of deja vu?" + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Tracer.png");
                    break;
                case 22:
                    await ReplyAsync("What's an aimbot?" + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Widowmaker.png");
                    break;
                case 23:
                    await ReplyAsync("Did someone say peanut butter?" + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Winston.png");
                    break;
                case 24:
                    await ReplyAsync("In russia game plays you." + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Zarya.png");
                    break;
                case 25:
                    await ReplyAsync("Experience tranquility" + "\n");
                    await Context.Channel.SendFileAsync("C:/img/Zenyatta.png");
                    break;
            }
        }


        [Command("Ping")]
        [Alias("ping", "pong")]
        [Summary("Returns a pong")]
        public async Task Say()
        {
            var rnd = new Random();
            int getal = rnd.Next(1, 6);
            int g1 = rnd.Next(1, 255);
            int g2 = rnd.Next(1, 255);
            int g3 = rnd.Next(1, 255);
            var embed = new EmbedBuilder()
            {
                Color = new Color(g1, g2, g3),
            };
            embed.Title = "Go fuck urself!" + ":middle_finger:";
            await ReplyAsync("", false, embed.Build());
        }

        string[] predictionsTexts = new string[]
            {
                "Yes",
                "No",
                "Idk",
                "Might be",
                "Yes, of course",
                "Aww hell naww"
            };

        Random rand = new Random();

        [Command("Ask")]
        [Summary("Gives a prediction")]
        [Alias("askme", "8ball")]
        public async Task EightBall([Remainder] string input)
        {
            int randomIndex = rand.Next(predictionsTexts.Length);
            string text = predictionsTexts[randomIndex];
            await ReplyAsync(Context.User.Mention + ", " + text);
        }

        string[] randomJoke = new string[]
        {
            "What does Mei say when taking pictures of people? \n Freeze don't move!",
            "What's Junkrat's favorite food? \n Spam.",
            "Why did Soldier: 76 get hired as a Chief Web Designer? \n Because he gets everyone in his sites.",
            "What's the difference between a good Pharah and a great one? \n Mercy....",
            "Why doesn't Lucio own a better gun? \n He broke it down.",
            "What is it called when you wake up and play League? \n Ryze and Shen.",
            "If Jax worked at a corporate job he'd be called Fax.",
            "Good Lux!, Have pun!",
            "What do you call a Bronze League player? \n Thijmen Hogenkamp."

        };

        [Command("Joke")]
        [Summary("Tells a random joke")]
        [Alias("funny")]
        public async Task Joke()
        {
            int RandomIndex = rand.Next(randomJoke.Length);
            string text = randomJoke[RandomIndex];
            await Context.Channel.SendMessageAsync(text);
         }

        [Command("nudes")]
        [Summary("tits")]
        [Alias("boobs", "rack")]
        [Remarks("Fetches some sexy titties")]
        public async Task BoobsAsync()
        {
            var GuildUser = await Context.Guild.GetUserAsync(Context.User.Id);
            if (!(GuildUser.Id == 250280353179238400))
            {
                await Context.Channel.SendMessageAsync("lel no");
            }
            else
            {
                JToken obj;
                var rnd = new Random().Next(0, 10229);
                using (var http = new HttpClient())
                {
                    obj = JArray.Parse(await http.GetStringAsync($"http://api.oboobs.ru/boobs/%7Brnd%7D%22"))[0];
                }
                var builder = new EmbedBuilder
                {
                    ImageUrl = $"http://media.oboobs.ru/%7Bobj[%22preview%22]%7D",
                    Description = $"Tits Database Size: 10229\n Image Number: {rnd}"
                };


                await ReplyAsync("", false, builder.Build());
            }
        }


        private CommandService _service;
        public General(CommandService service)
        {
            _service = service;
        }

        [Command("help")]
        [Remarks("Shows a list of all available commands per module.")]
        public async Task HelpAsync()
        {
            var dmChannel = await Context.User.GetOrCreateDMChannelAsync(); /* A channel is created so that the commands will be privately sent to the user, and not flood the chat. */

            string prefix = "!";  /* put your chosen prefix here */
            var builder = new EmbedBuilder()
            {
                Color = new Color(114, 137, 218),
                Description = "These are the commands you can use"
            };

            foreach (var module in _service.Modules) /* we are now going to loop through the modules taken from the service we initiated earlier ! */
            {
                string description = null;
                foreach (var cmd in module.Commands) /* and now we loop through all the commands per module aswell, oh my! */
                {
                    var result = await cmd.CheckPreconditionsAsync(Context); /* gotta check if they pass */
                    if (result.IsSuccess)
                        description += $"{prefix}{cmd.Aliases.First()}\n"; /* if they DO pass, we ADD that command's first alias (aka it's actual name) to the description tag of this embed */
                }

                if (!string.IsNullOrWhiteSpace(description)) /* if the module wasn't empty, we go and add a field where we drop all the data into! */
                {
                    builder.AddField(x =>
                    {
                        x.Name = module.Name;
                        x.Value = description;
                        x.IsInline = false;
                    });
                }
            }
            await dmChannel.SendMessageAsync("", false, builder.Build()); /* then we send it to the user. */
        }
    }
}