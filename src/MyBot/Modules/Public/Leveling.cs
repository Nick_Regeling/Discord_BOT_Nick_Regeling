﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace MyBot.Modules.Public
{
    public class Leveling : ModuleBase
    {
        [Command("Status")]
        [Alias("status")]
        public async Task Say([Remainder] IUser user = null)
        {
            if (user == null)
            {
                user = Context.User;
            }
            var tableName = Database.GetUserStatus(user);

            await ReplyAsync(Context.User.Mention + ", \n\n" + user.Username + "'s current status is as follows: \n" +
                            ":small_blue_diamond:" + "UserID: " + tableName.FirstOrDefault().UserId + "\n" +
                            ":small_blue_diamond:" + tableName.FirstOrDefault().Tokens + " to spend!");
        }


        [Command("award")]
        public async Task Award(SocketGuildUser user, [Remainder] int tokens)
        {
            var usere = await Context.Guild.GetUserAsync(Context.User.Id);

            if (!(usere.Id == 250280353179238400))
            {
                await Context.Channel.SendMessageAsync("Only non-weebs can award tokens.");
            }
            else
            {
                Database.ChangeTokens(user, tokens);
                await ReplyAsync(user.Mention + ", was awarded " + tokens + " tokens!");
            }
        }
    }
}