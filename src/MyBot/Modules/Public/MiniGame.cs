﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Collections.Generic;

namespace MyBot.Modules.Public
{

    public class Mini_Game : ModuleBase
    {
        static string gameLive = "";
        static string[] rWords = new string[11];
        static string woord = null;
        static string player;
        static string outputX;
        static string[] gokwoord;
        static int aantal;
        static int fouten;

        private async Task DelayDeleteMessage(IUserMessage message, int time = 0)
        {
            var delete = Task.Run(async () =>
            {
                if (time == 0) await Task.Delay(2500);
                else await Task.Delay(time * 1000);
                await message.DeleteAsync();
            });
        }

        [Command("MiniGame")]
        [Alias("mg")]
        [Summary("Starts a minigame")]
        public async Task miniGame()
        {
            int Delete = 1;
            foreach (var Item in await Context.Channel.GetMessagesAsync(Delete).Flatten())
            {
                await Item.DeleteAsync();
            }


            if (gameLive == "")
            {
                gameLive = "live";
                rWords[0] = "instruct";
                rWords[1] = "grateful";
                rWords[2] = "hilarious";
                rWords[3] = "enchanting";
                rWords[4] = "company";
                rWords[5] = "abstracted";
                rWords[6] = "functional";
                rWords[7] = "umbrella";
                rWords[8] = "zephyr";
                rWords[9] = "hottentottententententoonstelling";
                rWords[10] = "spaghetti";


                fouten = 0;
                var rand = new Random();
                int randomWord = rand.Next(rWords.Length);
                woord = Convert.ToString(rWords[randomWord]);
                player = Context.User.Mention;

                aantal = woord.Length;
                gokwoord = new string[aantal];
                for (int i = 0; i < aantal; i++)
                {
                    gokwoord[i] = ".";
                }

                gameLive = "live";
                var embed = new EmbedBuilder()
                {
                    Color = new Color(96, 255, 185)
                };
                embed.AddField(x =>
                {
                    x.Name = $"Game started!";
                    x.Value = $"Type !guess [Your guess] to guess an letter" + "\n" + "You have a maximum of 5 wrong guesses per game.";
                });
                var message = await ReplyAsync("", embed: embed);
            }
            else
            {
                var messageToDel = await ReplyAsync("Minigame already started, only one at a time please");
                return;
            }
        }

        [Command("Guess")]
        [Alias("g")]
        public async Task guessWord(char gChar)
        {
            var arrWoord = woord[0];
            string mesGuess = null;
            string mesFout = null;

            if (Context.User.Mention != player)
            {
                await Context.Channel.SendMessageAsync("This is not your game please wait.");
            }
            else
            {
                if (gameLive != "live")
                {
                    var message = await Context.Channel.SendMessageAsync("You can't guess without starting the minigame first.");
                    return;
                }
                else
                {
                    /* if (gChar)
                     {
                         var message2 = await Context.Channel.SendMessageAsync("You can only guess 1 letter a time.");
                         return;
                     }
                     else
                     {*/
                    outputX = null;
                    if (woord.ToLower().Contains(gChar))
                    {


                        for (int x = 0; x < aantal; x++)
                        {
                            if (woord[x] == gChar)
                            {
                                gokwoord[x] = gChar.ToString();
                            }
                            outputX += $"{gokwoord[x]} ";
                        }

                        mesGuess = "You guessed right!";

                    }
                    else
                    {
                        mesGuess = "You guessed wrong!";
                        fouten++;
                    }
                    if (fouten >= 5)
                    {
                        gameLive = "";
                        

                        var embedded = new EmbedBuilder()
                        {
                            Color = new Color(96, 255, 185)
                        };
                        embedded.AddField(x =>
                        {
                            x.Name = $"Game Over!";
                            x.Value = $"You have reached the maximum amount of 5 wrong guesses.";
                        });
                        await ReplyAsync("", embed: embedded);
                    }
                    else
                    {

                        var embed = new EmbedBuilder()
                        {
                            Color = new Color(96, 255, 185)
                        };
                        embed.Description = ($"{mesGuess}" + " " + $" {outputX}");
                        await ReplyAsync("", false, embed.Build());
                    }
                }
            }
            //}
        }
        /* static List<char>   alreadyGuessed = new List<char>() ;
         private bool alreadyguessed(char gChar)
         {
             foreach  (char k in alreadyGuessed)
             {
                if (k == gChar)
                 {
                     return true;
                 }
                 continue;
             }
             return false;
         }*/

        [Command("GuessWord")]
        [Alias("gw")]
        public async Task guessW(string gWord)
        {
            string mesGuessW = null;


            if (Context.User.Mention != player)
            {
                await Context.Channel.SendMessageAsync("This is not your game please wait.");
            }
            else
            {

                if (gameLive != "live")
                {
                    var mesW = await Context.Channel.SendMessageAsync("You need to start the minigame first.");
                    DelayDeleteMessage(Context.Message, 10);
                    DelayDeleteMessage(mesW, 10);
                }
                else
                {
                    if (gWord == woord)
                    {

                        var embedded = new EmbedBuilder();
                            embedded.Title = ($"Congratulations!");
                            embedded.Description = ($"You have guessed the word!" + "\n" + "You ended with " + $"{fouten}" + " wrong guesses.");
                            embedded.Color = new Color(96, 255, 185);
                        
                        await ReplyAsync("", false, embedded.Build());
                    }
                    else
                    {
                        mesGuessW = "I am afraid that is not correct.";
                        fouten++;
                        fouten++;
                        fouten++;

                        var embed = new EmbedBuilder()
                        {
                            Color = new Color(96, 255, 185)
                        };
                        embed.Description = ($"{mesGuessW}");
                        await ReplyAsync("", false, embed.Build());
                    }
                    if (fouten >= 5)
                    {
                        gameLive = "";


                        var embedded = new EmbedBuilder()
                        {
                            Color = new Color(96, 255, 185)
                        };
                        embedded.AddField(x =>
                        {
                            x.Name = $"Game Over!";
                            x.Value = $"You have reached the maximum amount of 5 wrong guesses.";
                        });
                        await ReplyAsync("", embed: embedded);
                    }
                }
            }
        }

        [Command("Stop")]
        [Alias("s")]
        public async Task stopGame()
        {
            if (Context.User.Mention != player)
            {
                await Context.Channel.SendMessageAsync("You are not allowed to stop this game.");
            }
            else
            {
                if (gameLive != "live")
                {
                    await Context.Channel.SendMessageAsync("There is no game I can stop.");
                }
                if (gameLive == "live")
                {
                    gameLive = "";
                    await Context.Channel.SendMessageAsync("You stopped the game");
                }
            }
        }
    }
}
