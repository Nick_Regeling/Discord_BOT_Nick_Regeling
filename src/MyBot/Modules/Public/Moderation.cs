﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace MyBot.Modules.Public
{
    public class Moderation : ModuleBase
    {
        [Command("Ban")]
        [Summary("Ban @Username")]
        [RequireUserPermission(GuildPermission.BanMembers)] ///Needed User Permissions ///
        [RequireBotPermission(GuildPermission.BanMembers)] ///Needed Bot Permissions ///
        public async Task BanAsync(SocketGuildUser user = null, [Remainder] string reason = null)
        {
            if (user == null) throw new ArgumentException("You must mention a user");
            if (string.IsNullOrWhiteSpace(reason)) throw new ArgumentException("You must provide a reason");

            var gld = Context.Guild as SocketGuild;
            var embed = new EmbedBuilder();
            embed.WithColor(new Color(0x4900ff));
            embed.Title = $"**{user.Username}** was banned";
            embed.Description = $"**Username: **{user.Username}\n**Guild Name: **{user.Guild.Name}\n**Banned by: **{Context.User.Mention}!\n**Reason: **{reason}";

            await gld.AddBanAsync(user);
            await Context.Channel.SendMessageAsync("", false, embed);
        }

        [Command("Kick")]
        [RequireBotPermission(GuildPermission.KickMembers)]
        [RequireUserPermission(GuildPermission.KickMembers)]
        public async Task KickAsync(SocketGuildUser user, [Remainder] string reason)
        {
            if (user == null) throw new ArgumentException("You must mention a user");
            if (string.IsNullOrWhiteSpace(reason)) throw new ArgumentException("You must provide a reason");

            var gld = Context.Guild as SocketGuild;
            var embed = new EmbedBuilder();
            embed.WithColor(new Color(0x4900ff));
            embed.Title = $" {user.Username} has been kicked from {user.Guild.Name}";
            embed.Description = $"**Username: **{user.Username}\n**Guild Name: **{user.Guild.Name}\n**Kicked by: **{Context.User.Mention}!\n**Reason: **{reason}";

            await user.KickAsync();
            await Context.Channel.SendMessageAsync("", false, embed);
        }

        [Command("SetGame")]
        [Alias("sg")]
        [Summary("Sets a 'Game' for the bot")]
        public async Task setgame([Remainder] string game)
        {
            var user = await Context.Guild.GetUserAsync(Context.User.Id);

            if (!(user.Id == 250280353179238400))
            {
                await Context.Channel.SendMessageAsync("Only non-weebs can make me play something else.");
            }
            else
            {
                await (Context.Client as DiscordSocketClient).SetGameAsync(game);
                await Context.Channel.SendMessageAsync($"I am now playing *{game}*");
                Console.WriteLine($"{DateTime.Now}: Game was changed to {game}");
            }
        }

        [Command("Cleanup")]
        [Alias("clean")]
        [Summary("Mass delete of messages")]
        [RequireBotPermission(GuildPermission.ManageMessages)]
        [RequireUserPermission(GuildPermission.ManageMessages)]
        public async Task cleanup()
        {
            var items = await Context.Channel.GetMessagesAsync().Flatten();
            await Context.Channel.DeleteMessagesAsync(items);
        }
    }
}
