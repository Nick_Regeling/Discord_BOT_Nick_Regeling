﻿using System;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using Discord.Commands;
using MyBot.Modules.Public;
using System.Linq;


namespace MyBot
{
    public class Program
    {
        // Convert our sync main to an async main.
        public static void Main(string[] args) =>
            new Program().Start().GetAwaiter().GetResult();

        private DiscordSocketClient _client;
        private CommandHandler _commands;

        public async Task Start()
        {
            _client = new DiscordSocketClient();
            _commands = new CommandHandler();


                await _client.LoginAsync(TokenType.Bot, "MzY1MDY4Nzk2NDY3MzQ3NDU2.DLeQRQ.zdDsjBTikOpZ2b9Mq21k4gmtoVM");
            await _client.StartAsync();

            _client.SetGameAsync($"use !Command");

            _client.Log += Log;
            _client.MessageReceived += giveXP;
            

            await _commands.Install(_client);

            await Task.Delay(-1);
        }

        private async Task giveXP(SocketMessage msg)
        {
            var user = msg.Author;
            var result = Database.CheckExistingUser(user);
            if (result.Count <= 0 && user.IsBot != true)
            {
                Database.EnterUser(user);
            }

            var userData = Database.GetUserStatus(user).FirstOrDefault();
            var xp = XP.returnXP(msg);
            var xpToLevelUp = XP.calculateNextLevel(userData.level);

            if (userData.XP >= xpToLevelUp)
            {
                Database.levelUp(user, xp);

                var embed = new EmbedBuilder();
                embed.WithColor(new Color(0x4d006d)).AddField(y =>
                {
                    var userData2 = Database.GetUserStatus(user).FirstOrDefault();
                    y.Name = "Leveled Up!";
                    y.Value = $"{user.Mention} has leveled up to level {userData2.level}!";
                });
                await msg.Channel.SendMessageAsync("", embed: embed);
            }
            else if (user.IsBot != true)
            {
                Database.addXP(user, xp);
            }
        }

        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }
    }
}